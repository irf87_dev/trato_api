const express = require('express'),
	bodyParser = require('body-parser'),
	path = require('path'),
	app = express();

const apiUri = require('./api/api_view');
const port = 8081;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");	
	//res.header("Access-Control-Allow-Headers", "Origin, x-session-token, x-session-origin, Content-Type, Accept");
	res.header('Access-Control-Allow-Credentials', true);
	next();
});

/* Rutas */
app.use('/api', apiUri);

app.listen(port);
console.log("Running on " + port);