const oper = {
	crateQuery : crateQuery,
	deleteQuery : deleteQuery,
	validate : validate,
	selectQuery : selectQuery,
	updateQuery : updateQuery
}

module.exports = oper;

function crateQuery(args){
	let sql = "INSERT INTO user (name, email, age)";
	sql += " VALUES ('" + args.name +"','" + args.email + "'," + args.age + ");";
	return sql;
}

function deleteQuery(id){
	let sql = "DELETE FROM user WHERE id = " + id;
	return sql;
}

function validate(args,isUpdate){
	let oValid = {
		isValid : true,
		message : "",
		cont : 0
	};
	let arrayKeys = ["name","email","age"];
	
	for(let i = 0; i < arrayKeys.length; i++){
		let key = arrayKeys[i];
		if(!isUpdate){
			if(!args[key]){
				oValid.isValid = false;
				oValid.message += "Property " +  key +" required, ";

			}
			if(key == "age"){
				if(typeof args[key] != "number"){
					oValid.isValid = false;
					oValid.message += "Property " +  key +" it must to be a number, ";
				}
			}
		}

		else{
			if(args[key]) oValid.cont ++;
			if(key == "age" && args[key]){
				if(typeof args[key] != "number"){
					oValid.isValid = false;
					oValid.message += "Property " +  key +" it must to be a number, ";
				}
			}
		}

	}
	oValid.message = oValid.message.replace(/, $/,".");
	return oValid;
}

function selectQuery(id){
	let sql = "SELECT * FROM user";
	if(id){
		sql += " WHERE id = " + id;
	}
	return sql
}

function updateQuery(args){
	let id = args.id;
	
	delete args.id;

	let sql = "UPDATE user SET ";
	let keys = Object.keys(args);
	for(let i = 0; i < keys.length ; i ++){
		let key = keys[i]
		sql +=  key + " = ";
		if(typeof args[key] == "string"){
			sql += "'" + args[key] + "',"
		}
		else sql += args[key] + ",";
	}
	sql = sql.replace(/,$/," ")
	sql += " WHERE id = " + id;
	return sql
}
