const mysql = require('mysql');
const bdName = "awstest";

//Production ENVIRONMENT
const con = mysql.createConnection({
  host: "aws-test.cm7d5lgfchgt.us-east-1.rds.amazonaws.com",
  user: "awstest",
  password: "awstest.2017",
  database : bdName
});
/*
//DEVELOP ENVIRONMENT
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database : bdName
});
*/
const oper = {
	runQuery : runQuery
}

con.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
  }
  else{
  	console.log('connected as id ' + con.threadId);
  	createTables();
  }
	
});

function runQuery(sql,callBack){
	con.query(sql, function (err, result) {
		if (err) callBack({error : err});
		else callBack( {result : result });
	});
}

function createTables(){
	let sql = `create table if not exists user(
		id int primary key auto_increment,
		name varchar(255)not null,
		email VARCHAR(50),
		age int not null default 0
	)`;
	runQuery(sql,(r)=>{
		console.log(r)
	});
}

module.exports = oper; 