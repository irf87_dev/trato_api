const bd = require('./bd_conections');
const model = require('./model');

const operation = {
	add: add,
	get : get,
	update : update,
	del : del
};

function get(args, callBack){
	bd.runQuery(model.selectQuery(args.id),callBack);
}

function add(args, callBack){
	try{
		if(!args) throw("Incorrect params");
		let oValid = model.validate(args, false);
		
		if(oValid.isValid){
			bd.runQuery(model.crateQuery(args),callBack);
		}
		else callBack({ error : oValid.message });

	}
	catch(err){
		console.error(err);
		callBack({error : err});
	}
}

function update(args,callBack){
	try{
		if(!args) throw("Empty request");
		if(!args.id) throw("Id property is required");
		let oValid = model.validate(args, true);
		
		if(oValid.isValid){
			if(oValid.cont > 0 ){
				bd.runQuery(model.updateQuery(args),callBack);
			}
			else throw("Nothing to update");
		}
		else callBack({ error : oValid.message });

	}
	catch(err){
		console.error(err);
		callBack({error : err});
	}
}

function del(args,callBack){
	try{
		if(!args.id) throw("Id property is required");
		bd.runQuery(model.deleteQuery(args.id),(r)=>{
			if(r.error) callBack(r);
			else{
				callBack({result : "Record " + args.id + " has been deleted"});
			}
		});
	}
	catch(err){
		callBack({error : err});
	}
}

module.exports = operation;