const express = require('express'),
	router = express.Router();

const controller = require('./api_controller');

router.get('/', function(req, res){
	controller.get(req.query,onRespond);

	function onRespond(oResp){
		if(oResp.error)res.status(401).send(oResp);
		else res.status(200).send(oResp);
	}
});

router.post('/',function(req, res){


	controller.add(req.body,onRespond);

	function onRespond(oResp){
		if(oResp.error)res.status(404).send(oResp);
		else res.status(200).send(oResp);
	}
});

router.put('/', function(req, res){

	controller.update(req.body,onRespond);

	function onRespond(resp){
		if(resp.error) res.status(400).send(resp);
		else{
			res.status(200).send(resp);
		}
	}
});

router.delete('/', function(req, res){
	controller.del(req.body,onRespond);

	function onRespond(resp){
		if(resp.error) res.status(400).send(resp);
		else{
			res.status(200).send(resp);
		}
	}
});


module.exports = router;
